package usyd.totalkids.web;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import usyd.totalkids.domain.Account;
import usyd.totalkids.domain.CalendarRecords;
import usyd.totalkids.domain.Child;
import usyd.totalkids.domain.ExerciseRecord;

@RequestMapping("/exerciserecords")
@Controller
@RooWebScaffold(path = "exerciserecords", formBackingObject = ExerciseRecord.class)
public class ExerciseRecordController {

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, HttpServletRequest httpServletRequest) {
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        uiModel.addAttribute("exerciseRecord", new ExerciseRecord());
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("children", Child.findChildrenByRaisedBy(raisedBy).getResultList());
        return "exerciserecords/create";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel, HttpServletRequest httpServletRequest) {
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        List<Child> ChildList = Child.findChildrenByRaisedBy(raisedBy).getResultList();
        String[] AverageTimes = new String[ChildList.size()];
        String[] advices = new String[ChildList.size()];
        List<ExerciseRecord> CalendarRecordesList = null;
        Date earliest = null;
        for (int i = 0; i < ChildList.size(); i++) {
            int time = 0;
            double AverageTime = 0;
            if (i == 0) {
                List<ExerciseRecord> templist = ExerciseRecord.findExerciseRecordsByForChild(ChildList.get(i)).getResultList();
                if (templist.size() == 0) continue;
                earliest = templist.get(i).getRecordDate();
                CalendarRecordesList = templist;
                for (ExerciseRecord c : templist) {
                    if (earliest.after(c.getRecordDate())) {
                        earliest = c.getRecordDate();
                    }
                    time += c.getExerciseTime();
                }
                Date today = new Date();
                long daysBetween = TimeUnit.MILLISECONDS.toDays(today.getTime() - earliest.getTime()) + 1;
                AverageTime = (double) time / (double) daysBetween;
                System.out.println(daysBetween);
            } else {
                List<ExerciseRecord> templist = ExerciseRecord.findExerciseRecordsByForChild(ChildList.get(i)).getResultList();
                if (templist.size() == 0) continue;
                earliest = templist.get(0).getRecordDate();
                CalendarRecordesList.addAll(templist);
                for (ExerciseRecord c : templist) {
                    if (earliest.after(c.getRecordDate())) {
                        earliest = c.getRecordDate();
                    }
                    time += c.getExerciseTime();
                }
                Date today = new Date();
                long daysBetween = TimeUnit.MILLISECONDS.toDays(today.getTime() - earliest.getTime()) + 1;
                AverageTime = (double) time / (double) daysBetween;
                System.out.println(daysBetween);
            }
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            AverageTimes[i] = "";
            AverageTimes[i] += "The average exercise time between today to " + sf.format(earliest) + " for Child " + ChildList.get(i).getFirstName() + " " + ChildList.get(i).getLastName() + " is: " + Double.valueOf(twoDForm.format(AverageTime)) + " hours";
            advices[i] = "";
            if (AverageTime < (double) 1) {
                advices[i] += "Here is the advice for your child " + ChildList.get(i).getFirstName() + " " + ChildList.get(i).getLastName() + ":\n" + "The average exersice time is less than 1 hours per day which means he/she need to do more exercise because the Children should get an hour of exercise over the course of each day. There is an article provide in Article menu which is called Children need an hour of exercise per day you can also go and have a look";
            } else {
                advices[i] += "Here is the advice for your child " + ChildList.get(i).getFirstName() + " " + ChildList.get(i).getLastName() + ":\n" + "The average exersice time is greater or equal than 1 hours per day, therefore your child is taking enough exercise for now have a good day.";
            }
        }
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("exerciserecords", CalendarRecordesList);
            float nrOfPages = (float) CalendarRecords.countCalendarRecordses() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
            uiModel.addAttribute("AverageTimes", AverageTimes);
            uiModel.addAttribute("Advices", advices);
            uiModel.addAttribute("earliest", earliest);
        } else {
            uiModel.addAttribute("exerciserecords", ExerciseRecord.findAllExerciseRecords());
        }
        addDateTimeFormatPatterns(uiModel);
        return "exerciserecords/list";
    }
}
