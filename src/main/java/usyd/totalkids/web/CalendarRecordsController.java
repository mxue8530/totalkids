package usyd.totalkids.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import usyd.totalkids.domain.Account;
import usyd.totalkids.domain.CalendarRecords;
import usyd.totalkids.domain.Child;

@RequestMapping("/calendarrecordses")
@Controller
@RooWebScaffold(path = "calendarrecordses", formBackingObject = CalendarRecords.class)
@RooWebFinder
public class CalendarRecordsController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid CalendarRecords calendarRecords, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, calendarRecords);
            return "calendarrecordses/create";
        }
        uiModel.asMap().clear();
        calendarRecords.persist();
        return "redirect:/calendarrecordses/" + encodeUrlPathSegment(calendarRecords.getId().toString(), httpServletRequest);
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, HttpServletRequest httpServletRequest) {
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        uiModel.addAttribute("calendarRecords", new CalendarRecords());
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("children", Child.findChildrenByRaisedBy(raisedBy).getResultList());
        return "calendarrecordses/create";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel, HttpServletRequest httpServletRequest) {
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        List<Child> ChildList = Child.findChildrenByRaisedBy(raisedBy).getResultList();
        String[] AverageTimes = new String[ChildList.size()];
        String[] advices = new String[ChildList.size()];
        List<CalendarRecords> CalendarRecordesList = null;
        Date earliest = null;
        for (int i = 0; i < ChildList.size(); i++) {
            int time = 0;
            double AverageTime = 0;
            if (i == 0) {
                List<CalendarRecords> templist = CalendarRecords.findCalendarRecordsesByForChild(ChildList.get(i)).getResultList();
                if (templist.size() == 0) continue;
                earliest = templist.get(i).getRecordDate();
                CalendarRecordesList = templist;
                for (CalendarRecords c : templist) {
                    if (earliest.after(c.getRecordDate())) {
                        earliest = c.getRecordDate();
                    }
                    time += c.getExerciseTime();
                }
                Date today = new Date();
                long daysBetween = TimeUnit.MILLISECONDS.toDays(today.getTime() - earliest.getTime());
                AverageTime = (double) time / (double) daysBetween;
                System.out.println(daysBetween);
            } else {
                List<CalendarRecords> templist = CalendarRecords.findCalendarRecordsesByForChild(ChildList.get(i)).getResultList();
                if (templist.size() == 0) continue;
                earliest = templist.get(0).getRecordDate();
                CalendarRecordesList.addAll(templist);
                for (CalendarRecords c : templist) {
                    if (earliest.after(c.getRecordDate())) {
                        earliest = c.getRecordDate();
                    }
                    time += c.getExerciseTime();
                }
                Date today = new Date();
                long daysBetween = TimeUnit.MILLISECONDS.toDays(today.getTime() - earliest.getTime());
                AverageTime = (double) time / (double) daysBetween;
                System.out.println(daysBetween);
            }
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd");
            AverageTimes[i] = "";
            AverageTimes[i] += "The average exercise time between today to " + sf.format(earliest) + " for Child " + ChildList.get(i).getFirstName() + " " + ChildList.get(i).getLastName() + " is: " + AverageTime + " hours";
            advices[i] = "";
            if (AverageTime < (double) 1) {
                advices[i] += "Here is the advice for your child " + ChildList.get(i).getFirstName() + " " + ChildList.get(i).getLastName() + ":\n" + "The average exersice time is less than 1 hours per day which means he/she need to do more exercise because the Children should get an hour of exercise over the course of each day. There is an article provide in Article menu which is called Children need an hour of exercise per day you can also go and have a look";
            } else {
                advices[i] += "Here is the advice for your child " + ChildList.get(i).getFirstName() + " " + ChildList.get(i).getLastName() + ":\n" + "The average exersice time is greater or equal than 1 hours per day, therefore your child is taking enough exercise for now have a good day.";
            }
        }
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("calendarrecordses", CalendarRecordesList);
            float nrOfPages = (float) CalendarRecords.countCalendarRecordses() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
            uiModel.addAttribute("AverageTimes", AverageTimes);
            uiModel.addAttribute("Advices", advices);
            uiModel.addAttribute("earliest", earliest);
        } else {
            uiModel.addAttribute("calendarrecordses", CalendarRecords.findAllCalendarRecordses());
        }
        addDateTimeFormatPatterns(uiModel);
        return "calendarrecordses/list";
    }
}
