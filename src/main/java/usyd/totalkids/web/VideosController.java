package usyd.totalkids.web;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import usyd.totalkids.domain.Videos;

@RequestMapping("/videoses")
@Controller
@RooWebScaffold(path = "videoses", formBackingObject = Videos.class)
public class VideosController {

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        Videos v = Videos.findVideos(id);
        String Video_Context = v.getVideo();
        uiModel.addAttribute("videos", Videos.findVideos(id));
        uiModel.addAttribute("video_context", Video_Context);
        uiModel.addAttribute("itemId", id);
        return "videoses/show";
    }
}
