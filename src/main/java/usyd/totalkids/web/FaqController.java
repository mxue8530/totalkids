package usyd.totalkids.web;

import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import usyd.totalkids.domain.Faq;

@RequestMapping("/faqs")
@Controller
@RooWebScaffold(path = "faqs", formBackingObject = Faq.class)
@RooWebFinder
public class FaqController {

    @RequestMapping(params = "find=ByQuestionLike", method = RequestMethod.GET)
    public String findFAQByQuestionLike(@RequestParam("Question") String question, Model uiModel) {
        uiModel.addAttribute("faqs", Faq.findFaqsByQuestionLike(question).getResultList());
        return "faqs/list";
    }
}
