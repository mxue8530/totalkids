package usyd.totalkids.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import usyd.totalkids.domain.Account;
import usyd.totalkids.domain.Child;
import usyd.totalkids.domain.Gender;
import usyd.totalkids.domain.NutritionCalendar;

@RequestMapping("/children")
@Controller
@RooWebScaffold(path = "children", formBackingObject = Child.class)
public class ChildController {

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder, WebDataBinder webDataBinder) throws ServletException {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, HttpServletRequest httpServletRequest) {
        uiModel.addAttribute("child", new Child());
        uiModel.addAttribute("loginName", httpServletRequest.getUserPrincipal().getName());
        uiModel.addAttribute("genders", Arrays.asList(Gender.values()));
        return "children/create";
    }

    @RequestMapping(params = "find=ByChildId", method = RequestMethod.GET, produces = "text/html")
    public String findChildByIdEquals(Model uiModel, @RequestParam("id2") String id2, HttpServletRequest httpServletRequest) {
        long id = Long.valueOf(id2);
        uiModel.addAttribute("children", Child.findChildrenByIdEquals(id).getSingleResult());
        return "redirect:/children/" + encodeUrlPathSegment(id2, httpServletRequest);
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Child child, BindingResult bindingResult, Model uiModel, @RequestParam("content") MultipartFile content, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, child);
            addDateTimeFormatPatterns(uiModel);
            return "children/create";
        }
        uiModel.asMap().clear();
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        child.setRaisedBy(raisedBy);
        child.persist();
        return "redirect:/children/" + encodeUrlPathSegment(child.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/list1", produces = "text/html")
    public String listChildrenByAccount(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel, HttpServletRequest httpServletRequest) {
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("children2", Child.findChildrenByRaisedBy(raisedBy).getResultList());
            float nrOfPages = (float) Child.countChildren() / sizeNo;
        } else {
        }
        addDateTimeFormatPatterns(uiModel);
        return "children/customList";
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        Child child = Child.findChild(id);
        child.setUrl("http://localhost:8080/TotalKids/children/showdoc/" + id);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("child", Child.findChild(id));
        uiModel.addAttribute("itemId", id);
        return "children/show";
    }

    @RequestMapping(value = "/showdoc/{id}", method = RequestMethod.GET)
    public String showdoc(@PathVariable("id") Long id, HttpServletResponse response, Model model) {
        Child child = Child.findChild(id);
        try {
            OutputStream out = response.getOutputStream();
            IOUtils.copy(new ByteArrayInputStream(child.getContent()), out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
