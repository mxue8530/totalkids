package usyd.totalkids.web;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import usyd.totalkids.domain.SmallGame;

@RequestMapping("/smallgames")
@Controller
@RooWebScaffold(path = "smallgames", formBackingObject = SmallGame.class)
public class SmallGameController {

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("smallgame", SmallGame.findSmallGame(id));
        uiModel.addAttribute("Game_context", SmallGame.findSmallGame(id).getGamePlay());
        uiModel.addAttribute("itemId", id);
        return "smallgames/show";
    }
}
