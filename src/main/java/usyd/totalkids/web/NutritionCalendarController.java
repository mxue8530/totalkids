package usyd.totalkids.web;

import java.awt.Font;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import usyd.totalkids.domain.Account;
import usyd.totalkids.domain.CalendarRecords;
import usyd.totalkids.domain.Child;
import usyd.totalkids.domain.NutritionCalendar;

@RequestMapping("/nutritioncalendars")
@Controller
@RooWebScaffold(path = "nutritioncalendars", formBackingObject = NutritionCalendar.class)
public class NutritionCalendarController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid NutritionCalendar nutritionCalendar, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, nutritionCalendar);
            return "nutritioncalendars/create";
        }
        uiModel.asMap().clear();
        nutritionCalendar.persist();
        return "redirect:/nutritioncalendars/" + encodeUrlPathSegment(nutritionCalendar.getId().toString(), httpServletRequest);
    }

    @RequestMapping(params = "find=ByChildId", method = RequestMethod.GET)
    public String listNutritionByThisChild(Model uiModel, @RequestParam("id2") String id2) {
        long id = Long.valueOf(id2);
        Child thisChild = Child.findChildrenByIdEquals(id).getSingleResult();
        int energy = 0;
        int days = 0;
        List<NutritionCalendar> templist = NutritionCalendar.findNutritionCalendarsByThisChild(thisChild).getResultList();
        for (NutritionCalendar c : templist) {
            energy += c.getEnergy();
            days++;
        }
        uiModel.addAttribute("Average", energy / days);
        uiModel.addAttribute("nutritioncalendars", NutritionCalendar.findNutritionCalendarsByThisChild(thisChild).getResultList());
        return "nutritioncalendars/list";
    }

    @RequestMapping(params = "find=ByNameLike", method = RequestMethod.GET)
    public String findNCByNameLike(@RequestParam("name") String name, Model uiModel, HttpServletRequest httpServletRequest) {
        List<Child> childList = Child.findChildrenByFirstNameLike(name).getResultList();
        childList.addAll(Child.findChildrenByLastNameLike(name).getResultList());
        childList.addAll(Child.findChildrenByMiddleNameLike(name).getResultList());
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        for (Child c : childList) {
            if ((c.getRaisedBy().getAccountName()).equals(raisedBy.getAccountName())) {
                uiModel.addAttribute("nutritioncalendars", NutritionCalendar.findNutritionCalendarsByThisChild(c).getResultList());
            }
        }
        return "nutritioncalendars/list";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel, HttpServletRequest httpServletRequest) {
        Account raisedBy = Account.findAccountsByAccountNameEquals(httpServletRequest.getUserPrincipal().getName()).getSingleResult();
        List<Child> ChildList = Child.findChildrenByRaisedBy(raisedBy).getResultList();
        List<NutritionCalendar> NutritionCalenderList = null;
        String[] energy2 = new String[ChildList.size()];
        int energy = 0;
        int days = 0;
        /*for (int i = 0; i < ChildList.size(); i++) {
            List<CalendarRecords> templist2 = CalendarRecords.findCalendarRecordsesByForChild(ChildList.get(i)).getResultList();
            List<NutritionCalendar> templist = NutritionCalendar.findNutritionCalendarsByThisChild(ChildList.get(i)).getResultList();
            NutritionCalenderList = templist;
            for (NutritionCalendar c : templist) {
                List<CalendarRecords> CR = CalendarRecords.findCalendarRecordsesByRecordDateEquals(c.getIntakeDate()).getResultList();
                if (CR.size() != 0) {
                    if ((c.getEnergy() - CR.get(0).getExerciseTime() * 100) > 1000) c.setSuggestion("Excess"); else if ((c.getEnergy() - CR.get(0).getExerciseTime() * 100) < 700) c.setSuggestion("Insufficiency"); else c.setSuggestion("Moderation");
                    c.persist();
                } else {
                    if (c.getEnergy() > 1000) c.setSuggestion("Excess"); else if (c.getEnergy() < 700) c.setSuggestion("Insufficiency"); else c.setSuggestion("Moderation");
                    c.persist();
                }
            }
        }*/
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("nutritioncalendars", NutritionCalendar.findNutritionCalendarEntries(firstResult, sizeNo));
            float nrOfPages = (float) NutritionCalendar.countNutritionCalendars() / sizeNo;
            uiModel.addAttribute("Average", energy2);
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("nutritioncalendars", NutritionCalendar.findAllNutritionCalendars());
        }
        addDateTimeFormatPatterns(uiModel);
        return "nutritioncalendars/list";
    }
}
