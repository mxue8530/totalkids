// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package usyd.totalkids.web;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;
import usyd.totalkids.domain.SmallGame;
import usyd.totalkids.web.SmallGameController;

privileged aspect SmallGameController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String SmallGameController.create(@Valid SmallGame smallGame, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, smallGame);
            return "smallgames/create";
        }
        uiModel.asMap().clear();
        smallGame.persist();
        return "redirect:/smallgames/" + encodeUrlPathSegment(smallGame.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String SmallGameController.createForm(Model uiModel) {
        populateEditForm(uiModel, new SmallGame());
        return "smallgames/create";
    }
    
    @RequestMapping(produces = "text/html")
    public String SmallGameController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("smallgames", SmallGame.findSmallGameEntries(firstResult, sizeNo));
            float nrOfPages = (float) SmallGame.countSmallGames() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("smallgames", SmallGame.findAllSmallGames());
        }
        return "smallgames/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String SmallGameController.update(@Valid SmallGame smallGame, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, smallGame);
            return "smallgames/update";
        }
        uiModel.asMap().clear();
        smallGame.merge();
        return "redirect:/smallgames/" + encodeUrlPathSegment(smallGame.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String SmallGameController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, SmallGame.findSmallGame(id));
        return "smallgames/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String SmallGameController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        SmallGame smallGame = SmallGame.findSmallGame(id);
        smallGame.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/smallgames";
    }
    
    void SmallGameController.populateEditForm(Model uiModel, SmallGame smallGame) {
        uiModel.addAttribute("smallGame", smallGame);
    }
    
    String SmallGameController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
