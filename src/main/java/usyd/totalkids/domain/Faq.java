package usyd.totalkids.domain;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findFaqsByQuestionLike" })
public class Faq {

    @NotNull
    private String Question;

    @NotNull
    private String Answer;
    
    @NotNull
    @Enumerated
    private Category Category;
}
