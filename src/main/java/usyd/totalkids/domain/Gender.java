package usyd.totalkids.domain;


public enum Gender {

    BOY, GIRL;
}
