package usyd.totalkids.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Article {

    @NotNull
    @Size(max = 300)
    private String Title;

    @NotNull
    @Size(max = 300)
    private String Author;

    @NotNull
    @Size(max = 9999)
    private String Context;
}
