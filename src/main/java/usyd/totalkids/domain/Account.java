package usyd.totalkids.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findAccountsByPassWordEquals", "findAccountsByAccountNameAndPassWordEquals", "findAccountsByAccountNameEquals" })
public class Account {

    @NotNull
    @Column(unique = true)
    @Size(min = 6, max = 20)
    private String accountName;

    @NotNull
    private String passWord;

    private String email;

    private String address;

    private int number;
}
