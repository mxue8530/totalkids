package usyd.totalkids.domain;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findChildrenByRaisedBy", "findChildrenByIdEquals", "findChildrenByFirstNameOrLastNameOrMiddleNameLike", "findChildrenByFirstNameLike", "findChildrenByMiddleNameLike", "findChildrenByLastNameLike", "findChildrenByFirstNameEquals" })
public class Child {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String middleName;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date DOB;

    @ManyToOne
    private Account raisedBy;

    @Enumerated(EnumType.STRING)
    private Gender Gender;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] content;

    @Transient
    @Size(max = 100)
    private String url;

}
