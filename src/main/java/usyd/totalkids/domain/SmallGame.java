package usyd.totalkids.domain;

import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class SmallGame {

    @Size(max = 100)
    private String GameName;

    @Size(max = 3000)
    private String GamePlay;
}
