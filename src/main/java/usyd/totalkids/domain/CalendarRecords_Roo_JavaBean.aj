// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package usyd.totalkids.domain;

import java.util.Date;
import usyd.totalkids.domain.CalendarRecords;
import usyd.totalkids.domain.Child;

privileged aspect CalendarRecords_Roo_JavaBean {
    
    public Date CalendarRecords.getRecordDate() {
        return this.RecordDate;
    }
    
    public void CalendarRecords.setRecordDate(Date RecordDate) {
        this.RecordDate = RecordDate;
    }
    
    public Child CalendarRecords.getForChild() {
        return this.ForChild;
    }
    
    public void CalendarRecords.setForChild(Child ForChild) {
        this.ForChild = ForChild;
    }
    
    public int CalendarRecords.getExerciseTime() {
        return this.ExerciseTime;
    }
    
    public void CalendarRecords.setExerciseTime(int ExerciseTime) {
        this.ExerciseTime = ExerciseTime;
    }
    
}
