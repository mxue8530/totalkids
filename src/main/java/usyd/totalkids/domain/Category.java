package usyd.totalkids.domain;


public enum Category {

    Animals, History, Food, Science, Earth, Sports, Math, Others;
}
