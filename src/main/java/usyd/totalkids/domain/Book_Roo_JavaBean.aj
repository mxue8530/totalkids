// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package usyd.totalkids.domain;

import usyd.totalkids.domain.Book;

privileged aspect Book_Roo_JavaBean {
    
    public String Book.getBookName() {
        return this.BookName;
    }
    
    public void Book.setBookName(String BookName) {
        this.BookName = BookName;
    }
    
}
