// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package usyd.totalkids.domain;

import java.util.Date;
import usyd.totalkids.domain.Child;
import usyd.totalkids.domain.NutritionCalendar;

privileged aspect NutritionCalendar_Roo_JavaBean {
    
    public Date NutritionCalendar.getIntakeDate() {
        return this.intakeDate;
    }
    
    public void NutritionCalendar.setIntakeDate(Date intakeDate) {
        this.intakeDate = intakeDate;
    }
    
    public int NutritionCalendar.getEnergy() {
        return this.energy;
    }
    
    public void NutritionCalendar.setEnergy(int energy) {
        this.energy = energy;
    }
    
    public Child NutritionCalendar.getThisChild() {
        return this.thisChild;
    }
    
    public void NutritionCalendar.setThisChild(Child thisChild) {
        this.thisChild = thisChild;
    }
    
    public String NutritionCalendar.getSuggestion() {
        return this.Suggestion;
    }
    
    public void NutritionCalendar.setSuggestion(String Suggestion) {
        this.Suggestion = Suggestion;
    }
    
}
