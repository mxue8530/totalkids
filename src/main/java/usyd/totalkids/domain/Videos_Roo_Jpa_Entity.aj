// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package usyd.totalkids.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import usyd.totalkids.domain.Videos;

privileged aspect Videos_Roo_Jpa_Entity {
    
    declare @type: Videos: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Videos.id;
    
    @Version
    @Column(name = "version")
    private Integer Videos.version;
    
    public Long Videos.getId() {
        return this.id;
    }
    
    public void Videos.setId(Long id) {
        this.id = id;
    }
    
    public Integer Videos.getVersion() {
        return this.version;
    }
    
    public void Videos.setVersion(Integer version) {
        this.version = version;
    }
    
}
