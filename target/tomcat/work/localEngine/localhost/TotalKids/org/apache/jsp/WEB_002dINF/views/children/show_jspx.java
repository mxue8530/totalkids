package org.apache.jsp.WEB_002dINF.views.children;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class show_jspx extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(4);
    _jspx_dependants.add("/WEB-INF/tags/form/show.tagx");
    _jspx_dependants.add("/WEB-INF/tags/util/panel.tagx");
    _jspx_dependants.add("/WEB-INF/tags/form/fields/display.tagx");
    _jspx_dependants.add("/WEB-INF/tags/form/fields/frame.tagx");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<div version=\"2.0\">");
      if (_jspx_meth_page_005fshow_005f0(_jspx_page_context))
        return;
      out.write("</div>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_page_005fshow_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  page:show
    org.apache.jsp.tag.web.form.show_tagx _jspx_th_page_005fshow_005f0 = new org.apache.jsp.tag.web.form.show_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_page_005fshow_005f0);
    _jspx_th_page_005fshow_005f0.setJspContext(_jspx_page_context);
    // /WEB-INF/views/children/show.jspx(5,120) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_page_005fshow_005f0.setZ("WtZLCKCwa15CFnlKC4eGi3puAYY=");
    // /WEB-INF/views/children/show.jspx(5,120) name = path type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_page_005fshow_005f0.setPath("/children");
    // /WEB-INF/views/children/show.jspx(5,120) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_page_005fshow_005f0.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(5,120) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_page_005fshow_005f0.setId("ps_usyd_totalkids_domain_Child");
    _jspx_th_page_005fshow_005f0.setJspBody(new Helper( 0, _jspx_page_context, _jspx_th_page_005fshow_005f0, null));
    _jspx_th_page_005fshow_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_page_005fshow_005f0);
    return false;
  }

  private boolean _jspx_meth_field_005fdisplay_005f0(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:display
    org.apache.jsp.tag.web.form.fields.display_tagx _jspx_th_field_005fdisplay_005f0 = new org.apache.jsp.tag.web.form.fields.display_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f0);
    _jspx_th_field_005fdisplay_005f0.setJspContext(_jspx_page_context);
    _jspx_th_field_005fdisplay_005f0.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(6,139) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f0.setZ("Nd/9HI5MfjcKfUs+aYwHJRZm19c=");
    // /WEB-INF/views/children/show.jspx(6,139) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f0.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(6,139) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f0.setId("s_usyd_totalkids_domain_Child_firstName");
    // /WEB-INF/views/children/show.jspx(6,139) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f0.setField("firstName");
    _jspx_th_field_005fdisplay_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f0);
    return false;
  }

  private boolean _jspx_meth_field_005fdisplay_005f1(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:display
    org.apache.jsp.tag.web.form.fields.display_tagx _jspx_th_field_005fdisplay_005f1 = new org.apache.jsp.tag.web.form.fields.display_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f1);
    _jspx_th_field_005fdisplay_005f1.setJspContext(_jspx_page_context);
    _jspx_th_field_005fdisplay_005f1.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(7,137) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f1.setZ("X84npA2MoKovDn1U0lLpmLhmIrc=");
    // /WEB-INF/views/children/show.jspx(7,137) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f1.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(7,137) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f1.setId("s_usyd_totalkids_domain_Child_lastName");
    // /WEB-INF/views/children/show.jspx(7,137) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f1.setField("lastName");
    _jspx_th_field_005fdisplay_005f1.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f1);
    return false;
  }

  private boolean _jspx_meth_field_005fdisplay_005f2(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:display
    org.apache.jsp.tag.web.form.fields.display_tagx _jspx_th_field_005fdisplay_005f2 = new org.apache.jsp.tag.web.form.fields.display_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f2);
    _jspx_th_field_005fdisplay_005f2.setJspContext(_jspx_page_context);
    _jspx_th_field_005fdisplay_005f2.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(8,141) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f2.setZ("ULmc6Wi1Lp6ymaN//6h/UmffyF4=");
    // /WEB-INF/views/children/show.jspx(8,141) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f2.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(8,141) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f2.setId("s_usyd_totalkids_domain_Child_middleName");
    // /WEB-INF/views/children/show.jspx(8,141) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f2.setField("middleName");
    _jspx_th_field_005fdisplay_005f2.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f2);
    return false;
  }

  private boolean _jspx_meth_field_005fdisplay_005f3(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:display
    org.apache.jsp.tag.web.form.fields.display_tagx _jspx_th_field_005fdisplay_005f3 = new org.apache.jsp.tag.web.form.fields.display_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f3);
    _jspx_th_field_005fdisplay_005f3.setJspContext(_jspx_page_context);
    _jspx_th_field_005fdisplay_005f3.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(9,182) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f3.setZ("icfLTfT+JOVE0DCXrc8vA70VkwU=");
    // /WEB-INF/views/children/show.jspx(9,182) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f3.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(9,182) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f3.setId("s_usyd_totalkids_domain_Child_DOB");
    // /WEB-INF/views/children/show.jspx(9,182) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f3.setField("DOB");
    // /WEB-INF/views/children/show.jspx(9,182) name = dateTimePattern type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f3.setDateTimePattern((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child_dob_date_format}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(9,182) name = date type = java.lang.Boolean reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f3.setDate(new Boolean(true));
    _jspx_th_field_005fdisplay_005f3.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f3);
    return false;
  }

  private boolean _jspx_meth_field_005fdisplay_005f4(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:display
    org.apache.jsp.tag.web.form.fields.display_tagx _jspx_th_field_005fdisplay_005f4 = new org.apache.jsp.tag.web.form.fields.display_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f4);
    _jspx_th_field_005fdisplay_005f4.setJspContext(_jspx_page_context);
    _jspx_th_field_005fdisplay_005f4.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(10,133) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f4.setZ("XHn0e4//zbQN+4Ft3EHrcxrR4/Y=");
    // /WEB-INF/views/children/show.jspx(10,133) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f4.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(10,133) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f4.setId("s_usyd_totalkids_domain_Child_Gender");
    // /WEB-INF/views/children/show.jspx(10,133) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f4.setField("gender");
    _jspx_th_field_005fdisplay_005f4.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f4);
    return false;
  }

  private boolean _jspx_meth_field_005fframe_005f0(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:frame
    org.apache.jsp.tag.web.form.fields.frame_tagx _jspx_th_field_005fframe_005f0 = new org.apache.jsp.tag.web.form.fields.frame_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fframe_005f0);
    _jspx_th_field_005fframe_005f0.setJspContext(_jspx_page_context);
    _jspx_th_field_005fframe_005f0.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(11,124) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fframe_005f0.setZ("user-managed");
    // /WEB-INF/views/children/show.jspx(11,124) name = render type = java.lang.Boolean reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fframe_005f0.setRender(new Boolean(true));
    // /WEB-INF/views/children/show.jspx(11,124) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fframe_005f0.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(11,124) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fframe_005f0.setId("s_usyd_totalkids_domain_Child_url");
    // /WEB-INF/views/children/show.jspx(11,124) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fframe_005f0.setField("url");
    _jspx_th_field_005fframe_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fframe_005f0);
    return false;
  }

  private boolean _jspx_meth_field_005fdisplay_005f5(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  field:display
    org.apache.jsp.tag.web.form.fields.display_tagx _jspx_th_field_005fdisplay_005f5 = new org.apache.jsp.tag.web.form.fields.display_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f5);
    _jspx_th_field_005fdisplay_005f5.setJspContext(_jspx_page_context);
    _jspx_th_field_005fdisplay_005f5.setParent(_jspx_parent);
    // /WEB-INF/views/children/show.jspx(12,136) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f5.setZ("5yRJ41xQ5DgFwe4zaUZuX4vbNPk=");
    // /WEB-INF/views/children/show.jspx(12,136) name = object type = java.lang.Object reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f5.setObject((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${child}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/children/show.jspx(12,136) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f5.setId("s_usyd_totalkids_domain_Child_content");
    // /WEB-INF/views/children/show.jspx(12,136) name = field type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_field_005fdisplay_005f5.setField("content");
    _jspx_th_field_005fdisplay_005f5.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_field_005fdisplay_005f5);
    return false;
  }

  private class Helper
      extends org.apache.jasper.runtime.JspFragmentHelper
  {
    private javax.servlet.jsp.tagext.JspTag _jspx_parent;
    private int[] _jspx_push_body_count;

    public Helper( int discriminator, JspContext jspContext, javax.servlet.jsp.tagext.JspTag _jspx_parent, int[] _jspx_push_body_count ) {
      super( discriminator, jspContext, _jspx_parent );
      this._jspx_parent = _jspx_parent;
      this._jspx_push_body_count = _jspx_push_body_count;
    }
    public boolean invoke0( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_field_005fdisplay_005f0(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_field_005fdisplay_005f1(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_field_005fdisplay_005f2(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_field_005fdisplay_005f3(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_field_005fdisplay_005f4(_jspx_parent, _jspx_page_context))
        return true;
      out.write("\n");
      out.write("=        ");
      if (_jspx_meth_field_005fframe_005f0(_jspx_parent, _jspx_page_context))
        return true;
      out.write("\n");
      out.write("=        ");
      if (_jspx_meth_field_005fdisplay_005f5(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public void invoke( java.io.Writer writer )
      throws JspException
    {
      JspWriter out = null;
      if( writer != null ) {
        out = this.jspContext.pushBody(writer);
      } else {
        out = this.jspContext.getOut();
      }
      try {
        this.jspContext.getELContext().putContext(JspContext.class,this.jspContext);
        switch( this.discriminator ) {
          case 0:
            invoke0( out );
            break;
        }
      }
      catch( Throwable e ) {
        if (e instanceof SkipPageException)
            throw (SkipPageException) e;
        throw new JspException( e );
      }
      finally {
        if( writer != null ) {
          this.jspContext.popBody();
        }
      }
    }
  }
}
